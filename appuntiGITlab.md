﻿
# Appunti GitLab: Comandi Git

Ecco i miei appunti sui comandi Git e GitLab per consultazioni rapide. Ricordiamo che Git è un sistema di controllo delle versioni e GitLab è un servizio che ci permette di ospitare i nostri repository Git.

## Configurazione Git

1.  Configura Git con il tuo nome:
    
    `git config --global user.name "Il tuo nome"`
    
2.  Configura Git con la tua email:
    
    `git config --global user.email "la.tua@email.com"`
    

## Creazione o Clonazione Repository

1.  Crea un nuovo repository:
    
    `git init`
    
2.  Clona un repository esistente:
    
    `git clone URL_REPO`
    

## Aggiungi, Fai Commit e Push

1.  Aggiungi i file modificati all'area di stage:
    
    `git add .`
    
2.  Fai un commit con un messaggio descrittivo:
    
    `git commit -m "Il tuo messaggio"`
    
3.  Manda le modifiche al repository remoto:
    
    `git push`
    

## Gestione Branch e Merge

1.  Crea un nuovo branch e passa a esso:
    
    `git checkout -b nome_branch`
    
2.  Torna al branch principale (di solito `main` o `master`):
    
    `git checkout main`
    
3.  Unisci il branch creato al branch principale:
    
    `git merge nome_branch`

## Aggiornare il Repository Locale

1.  Per scaricare gli aggiornamenti dal repository remoto, usa:
    
    `git fetch`
    
2.  Per unire gli aggiornamenti scaricati con il tuo branch locale, usa:
    
    `git merge`
    
3.  Puoi anche fare entrambe le operazioni in un solo comando con:
    
    `git pull`
    

## Risolvere Conflitti di Merge

1.  Quando ci sono conflitti dopo un merge, Git ti avvisa. Apri i file in conflitto e cerca i seguenti marcatori:
"<<<<<<<<
=========
<<<<<<<<<"

2.  Modifica i file per risolvere i conflitti manualmente e salva le modifiche.
    
3.  Per aggiungere i file modificati all'area di stage e fare un commit, usa:
`git add`
`git commit -m "Conflitti risolti e merge completato"`

#   
Appunti GitLab: Comandi Git (continuazione)

Continuiamo con altri appunti sui comandi Git per consultazioni rapide.

## Aggiornare il Repository Locale

1.  Per scaricare gli aggiornamenti dal repository remoto, usa:
    
    `git fetch`
    
2.  Per unire gli aggiornamenti scaricati con il tuo branch locale, usa:
    
    `git merge`
    
3.  Puoi anche fare entrambe le operazioni in un solo comando con:
    
    `git pull`
    

## Risolvere Conflitti di Merge

1.  Quando ci sono conflitti dopo un merge, Git ti avvisa. Apri i file in conflitto e cerca i seguenti marcatori:
    
    markdownCopy code
    
    `<<<<<<<
    =======
    >>>>>>>` 
    
2.  Modifica i file per risolvere i conflitti manualmente e salva le modifiche.
    
3.  Per aggiungere i file modificati all'area di stage e fare un commit, usa:
    
    sqlCopy code
    
    `git add .
    git commit -m "Conflitti risolti e merge completato"` 
    

## Visualizzare lo Storico dei Commit

1.  Per vedere la cronologia dei commit, usa:
    
    `git log`
    
2.  Per una versione più breve e leggibile, prova:
    
    `git log --oneline`
    

## Annullare Cambiamenti

1.  Per annullare le modifiche a un file, ripristinando la versione nel tuo ultimo commit, usa:
    
    `git checkout -- nome_file`
    
2.  Per annullare l'ultimo commit e mantenere le modifiche nel tuo ambiente di lavoro, usa:
    
    `git reset HEAD~`
    
3.  Per annullare l'ultimo commit e eliminare le modifiche, usa:
    
    `git reset --hard HEAD~`
    

## Collaborazione e Pull Request

1.  Per configurare il repository remoto, usa:
    
    `git remote add origin URL_REPO`
    
2.  Per mandare il tuo branch al repository remoto, usa:
    
    `git push -u origin nome_branch`
    
3.  Su GitLab, crea una Pull Request per proporre le modifiche al branch principale.

